/*
 * Author: Vince Belanger
 * CPS 151
 * September 19, 2016
 */
package lab5;

import java.util.Scanner;

public class FireCannonball
{
   public static void main(String[] args)
   {
      System.out.println("Please enter a starting angle: ");
      Scanner in = new Scanner(System.in);
      double angle = in.nextDouble();

      System.out.println("Please enter a firing velocity: ");
      double velocity = in.nextDouble();

      Cannonball ball = new Cannonball(0.0, 0.0);
      ball.shoot(angle, velocity);
   } 
}