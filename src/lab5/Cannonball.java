/*
 * Author: Vince Belanger
 * CPS 151
 * September 19, 2016
 */
package lab5;

import java.text.DecimalFormat;

public class Cannonball {
    private double xPos;
    private double yPos;
    private double xVel;
    private double yVel;
    final double GRAVITY = -9.81;
    DecimalFormat twoPoints = new DecimalFormat("0.00");
    
    public Cannonball(double xPos, double yPos){
        this.xPos = xPos;
        this.yPos = yPos;
        xVel = 0;
        yVel = 0;
    }
    
    private void move(double sec){
        double xDistance = (sec * xVel);
        double yDistance = (sec * yVel);
        yVel = yVel + (GRAVITY * sec);
        xPos = xPos + xDistance;
        yPos = yPos + yDistance;
    }
    
    public void shoot(double a, double v){
        double aRad = Math.toRadians(a);
        xVel = v * (Math.cos(aRad));
        yVel = v * (Math.sin(aRad));
        while(yPos >= 0){
            move(0.1);
            System.out.println("xpos: " + twoPoints.format(xPos) + ", ypos: " + twoPoints.format(yPos));
        }
        
    }
    
}
